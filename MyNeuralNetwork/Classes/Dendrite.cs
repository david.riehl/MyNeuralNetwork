﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeuralNetwork.Classes
{
    class Dendrite
    {
        // Static Properties

        private static Random random;
        private static Random Random
        {
            get
            {
                if (random == null)
                {
                    DateTime now = DateTime.Now;
                    int seed = now.Year + now.Month + now.Day + now.Hour + now.Minute + now.Second + now.Millisecond;
                    random = new Random(seed);
                }
                return random;
            }
        }

        // Properties

        /// <summary>
        /// Weight of the synapse
        /// </summary>
        public double SynapticWeight { get; private set; }

        /// <summary>
        /// Delta for next dendrite's Weight
        /// </summary>
        public double DeltaWeight { get; private set; }

        /// <summary>
        /// Weighted value from the dendrite (linked axon) to the neuron
        /// </summary>
        public double WeightedValue { get { return LinkedAxon.Value * SynapticWeight; } }

        /// <summary>
        /// Weighted gradient from the neuron to the dendrite
        /// </summary>
        public double WeightedGradient { get { return Neuron.Axon.Gradient * SynapticWeight; } }

        // Association(s)

        /// <summary>
        /// Neuron owner of the dendrite
        /// </summary>
        public Neuron Neuron { get; private set; }

        /// <summary>
        /// Axon linked to the dendrite
        /// </summary>
        public Axon LinkedAxon { get; private set; }

        // Constructor(s)

        public Dendrite(Neuron neuron, Axon linkedAxon)
        {
            Neuron = neuron;
            LinkedAxon = linkedAxon;
            SynapticWeight = RandomWeight();
        }

        // Static Method(s)

        /// <summary>
        /// Generate first random weight value
        /// </summary>
        /// <returns></returns>
        private static double RandomWeight()
        {
            double result;
            result = Random.Next(0, int.MaxValue) / (double)int.MaxValue;
            return result;
        }

        // Method(s)

        public void UpdateWeight()
        {
            // previous value of DeltaWeight
            double oldDeltaWeight = DeltaWeight;

            // new value of DeltaWeight
            double newDeltaWeight =
                // original output value, magnified by the neuron gradient and the learn rate
                LinkedAxon.Value * Neuron.Axon.Gradient * Neuron.Layer.Network.LearnRate
                // adding momentum = a fraction of the previous delta weight
                + Neuron.Layer.Network.Momentum * oldDeltaWeight;

            // Updating DeltaWeight
            DeltaWeight = newDeltaWeight;
            // Updating Weight
            SynapticWeight += newDeltaWeight;
        }
    }
}
