﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyNeuralNetwork.Classes;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;

namespace MyNeuralNetwork.Tools
{
    public class NetworkConfiguration
    {
        public string Title { get; set; }
        public Topology Topology { get; set; }
        public double LearnRate { get; set; }
        public double Momentum { get; set; }
        public string ActivationFunctionName { get; set; }
        public TrainingConfiguration TrainingConfiguration { get; set; }

        public static NetworkConfiguration LoadConfigurationFile(string path)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(NetworkConfiguration));
            NetworkConfiguration netConfig;

            using (FileStream stream = File.Open(path, FileMode.Open))
            {
                netConfig = (NetworkConfiguration)serializer.Deserialize(stream);
            }

            return netConfig;
        }

        public static void CreateAndGateConfigurationFile(string path = @"AND-Gate.xml")
        {
            XmlSerializer serializer = new XmlSerializer(typeof(NetworkConfiguration));
            NetworkConfiguration netConfig = new NetworkConfiguration()
            {
                Title = "AND-Gate",
                Topology = new Topology() { 2, 2, 1 },
                Momentum = 0.5,
                LearnRate = 0.2,
                ActivationFunctionName = "Tanh",
                TrainingConfiguration = new TrainingConfiguration()
                {
                    MaxEpochs = 5000,
                    TargetedAverageError = 0.9,
                    ValidationDataSet = new ValidationDataSet()
                    {
                        new ValidationData()
                        {
                            InputValues = new List<double>(){0,0},
                            ExpectedOutputValues = new List<double>() {0}
                        },
                        new ValidationData()
                        {
                            InputValues = new List<double>(){0,1},
                            ExpectedOutputValues = new List<double>() {0}
                        },
                        new ValidationData()
                        {
                            InputValues = new List<double>(){1,0},
                            ExpectedOutputValues = new List<double>() {0}
                        },
                        new ValidationData()
                        {
                            InputValues = new List<double>(){1,1},
                            ExpectedOutputValues = new List<double>() {1}
                        },
                    },
                },
            };
            FileStream stream = new FileStream(path, FileMode.Create);
            serializer.Serialize(stream, netConfig);
            stream.Close();
        }
    }
}
