﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNeuralNetwork.Tools
{
    public class ValidationData
    {
        public List<double> InputValues { get; set; }
        public List<double> ExpectedOutputValues { get; set; }
    }
}
